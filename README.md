# Ansible role to manage entropy tools

## Introduction

This role detects if a RNG hardware is present and supported, then install,
if required, the RNG tools to properly use it.

In order to support a RNG hardware, a specific driver that plugs into
[hw_random framework](https://www.kernel.org/doc/Documentation/hw_random.txt)
is required and must be loaded.

When `khwrngd` (the in-kernel equivalent of user-space rngd) is supported and
is able to use the hardware, RNG tools aren't needed, thus not installed.

Some related documentations:
* [Red Hat Enterprise Linux Virtual Machines: Access to Random Numbers Made Easy](https://rhelblog.redhat.com/2015/03/09/red-hat-enterprise-linux-virtual-machines-access-to-random-numbers-made-easy/)
* ['Enhanced Entropy Management in hwrng' section in RHEL 7.1 release notes](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/pdf/7.1_Release_Notes/Red_Hat_Enterprise_Linux-7-7.1_Release_Notes-en-US.pdf)
* [RHEL 7 - Virtualization Deployment and Administration Guide - Random Number Generator Device](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Virtualization_Deployment_and_Administration_Guide/sect-Guest_virtual_machine_device_configuration-Random_number_generator_device.html)
* [RHEL 6 - Virtualization Administration Guide - Random Number Generator (RNG) Device](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Virtualization_Administration_Guide/sect-Guest_virtual_machine_device_configuration-Random_number_generator_device.html)

When no hardware is available, in order to avoid entropy depletion,
it falls back to using the software generator
[Haveged](https://www.issihosts.com/haveged/).

## Variables

none

